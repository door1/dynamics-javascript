﻿function form_onLoad(executionContext) {
    var formContext = executionContext.getFormContext();

    var lostState = 2;

    var opportunityStateCode = formContext.getAttribute("opportunitystatecode").getValue();

    console.log("opportunitystatecode " + opportunityStateCode);

    // we only care about a Lost status
    if (opportunityStateCode !== lostState) {
        return;
    }

    var cancelled = 4;

    formContext.getControl("opportunitystatuscode").removeOption(cancelled);

    //-----------------------------------------------------------------------------------------------------
    // This code was once used to restrict the number of choices for Closed as Lost.
    // The decision was reversed and the functionality removed but it may be useful
    // in the future.
    //=====================================================================================================
    //var expired = 536260028;
    //var sellerTermination = 536260033;

    //var lostStatusReasons = formContext.getControl("opportunitystatuscode").getOptions();

    //for (var i = 0; i <= lostStatusReasons.length; i++) {

    //    if (lostStatusReasons[i] == undefined) {
    //        continue;
    //    }

    //    if (lostStatusReasons[i].value !== expired && lostStatusReasons[i].value !== sellerTermination) {
    //        formContext.getControl("opportunitystatuscode").removeOption(lostStatusReasons[i].value);
    //    }
    //}
}