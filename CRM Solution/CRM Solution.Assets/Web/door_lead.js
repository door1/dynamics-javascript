﻿/// <reference path="door_common_ui.js" />

async function onLoad(executionContext) {
    const formContext = executionContext.getFormContext();

    DoorUi.initialize(executionContext);

    formContext.getAttribute("door_leadtype").fireOnChange();
}

function onSave(executionContext) {
    const formContext = executionContext.getFormContext();

    const householdName = formContext.getAttribute("firstname").getValue() + " " + formContext.getAttribute("lastname").getValue() + " household";

    formContext.getAttribute("companyname").setValue(householdName);
    formContext.getAttribute("companyname").setSubmitMode("dirty");
}

function processBuyerSeller(executionContext) {
    const formContext = executionContext.getFormContext();

    const leadType = formContext.getAttribute("door_leadtype").getValue();

    const buyer = 100000000;
    const seller = 100000001;

    switch (leadType) {
        case buyer:
            DoorUi.hideField("door_initialvaluation");
            DoorUi.showTab("tab_buyer");
            DoorUi.hideTab("tab_seller");
            DoorUi.hideTab("tab_calendly");
            DoorUi.hideSection("Summary", "Summary_house_value");
            DoorUi.showSection("Summary", "Summary_buyernotes");
            break;
        case seller:
            DoorUi.showField("door_initialvaluation");
            DoorUi.showTab("tab_seller");
            DoorUi.showTab("tab_calendly");
            DoorUi.hideTab("tab_buyer");
            DoorUi.showSection("Summary", "Summary_house_value");
            DoorUi.hideSection("Summary", "Summary_buyernotes");
            break;
        default:
            break;
    }
}

function verifyStateAbbreviation(executionContext) {
    const formContext = executionContext.getFormContext();

    const states = ["AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "FL", "GA", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY", "DC"];

    var state = formContext.getAttribute("address1_stateorprovince").getValue();

    if (!state) {
        formContext.ui.clearFormNotification("001");

        return;
    }

    state = state.toUpperCase();

    const stateControl = formContext.getControl("address1_stateorprovince");

    formContext.getAttribute("address1_stateorprovince").setValue(state);

    if (states.indexOf(state) === -1) {
        formContext.ui.setFormNotification(state + " is not a valid two-letter state abbreviation", "ERROR", "001");

        const actionCollection = {
            message: "VALIDATION ERROR",
            actions: null
        };

        stateControl.addNotification({
            messages: [state + " is not a valid two-letter state abbreviation"],
            notificationLevel: "ERROR",
            uniqueId: "002",
            actions: [actionCollection]
        });

    } else {
        formContext.ui.clearFormNotification("001");
        stateControl.clearNotification("002");
    }
}
