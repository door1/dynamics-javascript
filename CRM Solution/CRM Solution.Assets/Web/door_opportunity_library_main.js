﻿/// <reference path="door_common2.js" />
/// <reference path="door_common_ui.js" />

async function opportunity_form_onLoad(executionContext) {
    const formContext = executionContext.getFormContext();

    DoorUi.initialize(executionContext);
    await DoorSecurity.initialize(executionContext);
    
    // this code is necessary because the customerid field is special
    // and you can't change the requirement level at the field-level
    DoorUi.setFieldNotRequired("customerid");

    const currentUser = {
        isLa: DoorSecurity.userHasRole("Door - LA"),
        isMa: DoorSecurity.userHasRole("Door - MA"),
        isManagerial: DoorSecurity.userHasRole("Door - Managerial"),
        isBuyingAgentLead: DoorSecurity.userHasRole("Door Buying Agent Lead")
    };

    const tabSummary = "tab_summary";
    const tabShowings = "tab_showings";
    const tabServiceJobs = "tab_servicejobs";
    const tabSellerPortal = "tab_sellerportal";
    const tabOffers = "tab_offers";
    const tabMa = "tab_ma";
    const tabOnboarding = "tab_onboarding";
    const tabTraffic = "tab_traffic";
    const tabTimeline = "tab_timeline";
    const buyer = 100000000;
    const seller = 100000001;

    const opportunityType = formContext.getAttribute("door_opportunitytype1").getValue();

    switch (opportunityType) {
        case buyer:
            DoorUi.hideTab(tabSellerPortal);
            DoorUi.hideTab(tabOnboarding);
            DoorUi.hideTab(tabSellerPortal);
            DoorUi.hideTab(tabTraffic);
            DoorUi.setTabFocus(tabTimeline);
            break;
        case seller:
            if (currentUser.isMa) {
                DoorUi.hideTab(tabServiceJobs);
                DoorUi.hideTab(tabOffers);
                DoorUi.hideTab(tabTraffic);

                setCurrentTabForMa(formContext);
            } else if (currentUser.isLa) {
                DoorUi.hideTab(tabOnboarding);
                DoorUi.hideTab(tabServiceJobs);
                DoorUi.hideTab(tabMa);
               // DoorUi.hideTab(tabSellerPortal);
            } else {
                DoorUi.showTab(tabSellerPortal);
            }
            break;
    }

    DoorUi.setSectionVisibility(tabSummary, "tab_general_section_doorpartners", opportunityType === seller);
    DoorUi.setSectionVisibility(tabSummary, "tab_general_section_listpricedetails", opportunityType === seller);
    DoorUi.setSectionVisibility(tabSummary, "tab_general_section_listingagreementdetails", opportunityType === seller);
    DoorUi.setSectionVisibility(tabSummary, "tab_general_section_agentlistinglive", opportunityType === seller);
    DoorUi.setSectionVisibility(tabSummary, "tab_general_section_addressinformation", opportunityType === seller);

    DoorUi.setFieldIsReadOnly("header_statuscode", !currentUser.isBuyingAgentLead);

    formContext.getAttribute("door_schedulingisautomated").fireOnChange();
    formContext.getAttribute("door_gatecode").fireOnChange();
    formContext.getAttribute("door_havepetsonproperty").fireOnChange();
    formContext.getAttribute("door_ibuyer").fireOnChange();
    formContext.getAttribute("door_renovation").fireOnChange();
    formContext.getAttribute("door_promotionalservice").fireOnChange();

    //var createdOn = formContext.getAttribute("footer_createdon").getValue();
    //debugger;
    //Xrm.Navigation.openAlertDialog({ text: createdOn });
}

function setCurrentTabForMa(formContext) {
    const selectedStage = formContext.data.process.getSelectedStage();

    if (selectedStage == null) {
        return;
    }

    const selectedStageName = selectedStage.getName();

    console.log(`Selected Stage: ${selectedStageName}`);

    var tabName = "tab_summary";

    switch (selectedStageName) {
        case "Listing Presentation Follow Up":
            tabName = "tab_ma";
            break;
        case "Onboarding":
            tabName = "tab_onboarding";
            break;
        default:
            tabName = "tab_summary";
            break;
    }

    DoorUi.setTabFocus(tabName);
}

function promotionalservice_onChange(executionContext) {
    const formContext = executionContext.getFormContext();

    const value = formContext.getAttribute("door_promotionalservice").getValue();

    const other = 536260006;

    DoorUi.setFieldVisibility("door_additionaldetails", value === other);
}

function iBuyer_onChange(executionContext) {
    const formContext = executionContext.getFormContext();

    const value = formContext.getAttribute("door_ibuyer").getValue();

    DoorUi.setFieldVisibility("door_ibuyeroffered", value != null);
    DoorUi.setFieldVisibility("door_clientaccepted", value != null);
}

function renovation_onChange(executionContext) {
    const formContext = executionContext.getFormContext();

    const value = formContext.getAttribute("door_renovation").getValue();

    DoorUi.setFieldVisibility("door_renovationoffered", value != null);
    DoorUi.setFieldVisibility("door_renovationcomplete", value != null);
}

function havePetsOnProperty_onChange(executionContext) {
    const formContext = executionContext.getFormContext();

    const value = formContext.getAttribute("door_havepetsonproperty").getValue();

    const yes = 100000000;

    DoorUi.setFieldVisibility("door_petdetails", value === yes);
}

function haveAnAlarm_onChange(executionContext) {
    const formContext = executionContext.getFormContext();

    const value = formContext.getAttribute("door_haveanalarm").getValue();

    const yes = 100000000;

    DoorUi.setFieldVisibility("door_disarm", value === yes);
}

function schedulingIsAutomated_onChange(executionContext) {
    const formContext = executionContext.getFormContext();

    const value = formContext.getAttribute("door_schedulingisautomated").getValue();

    DoorUi.setFieldVisibility("door_clientavailability1", value);
    DoorUi.setFieldVisibility("door_photovendorstatus", value);
    DoorUi.setFieldVisibility("door_stagingvendor", !value);
    DoorUi.setFieldVisibility("door_stagingdate", !value);
    DoorUi.setFieldVisibility("door_photographyvendor", !value);
    DoorUi.setFieldVisibility("door_photographydate", !value);
}

function listingLiveDate_onChange(executionContext) {
    const formContext = executionContext.getFormContext();

    const value = formContext.getAttribute("door_listinglivedate").getValue();

    DoorUi.setFieldIsReadOnly("door_acceptedoffer", value == null);
}