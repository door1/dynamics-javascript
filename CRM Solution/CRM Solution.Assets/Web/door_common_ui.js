﻿/// <reference path="intellisensefiles/xrm.d.ts" />

var DoorUi = window.DoorUi || {};

(function () {
    this.formContext = null;

    this.initialize = function (executionContext) {
        this.formContext = executionContext.getFormContext();
    }

    //#region - Section methods
    this.setSectionVisibility = function (tabName, sectionName, visible) {
        const tab = this.formContext.ui.tabs.get(tabName);

        if (tab == null) {
            return;
        }

        const section = tab.sections.get(sectionName);

        if (section == null) {
            return;
        }

        section.setVisible(visible);
    }

    this.showSection = function (tabName, sectionName) {
        const tab = this.formContext.ui.tabs.get(tabName);

        if (tab == null) {
            return;
        }

        const section = tab.sections.get(sectionName);

        if (section == null) {
            return;
        }

        section.setVisible(true);
    }

    this.hideSection = function (tabName, sectionName) {
        const tab = this.formContext.ui.tabs.get(tabName);

        if (tab == null) {
            return;
        }

        const section = tab.sections.get(sectionName);

        if (section == null) {
            return;
        }

        section.setVisible(false);
    }
    //#endregion

    //#region - Tab methods
    this.setTabVisibility = function (tabName, visible) {
        const tab = this.formContext.ui.tabs.get(tabName);

        if (tab == null) {
            return;
        }

        tab.setVisible(visible);
    }

    this.showTab = function (tabName) {
        const tab = this.formContext.ui.tabs.get(tabName);

        if (tab == null) {
            return;
        }

        tab.setVisible(true);
    }

    this.hideTab = function (tabName) {
        const tab = this.formContext.ui.tabs.get(tabName);

        if (tab == null) {
            return;
        }

        tab.setVisible(false);
    }

    this.setTabFocus = function (tabName) {
        const tab = this.formContext.ui.tabs.get(tabName);

        if (tab == null) {
            return;
        }

        tab.setFocus();
    }
    //#endregion

    //#region - Field Methods
    this.setFieldVisibilityForAllControls = function (fieldName, visible) {
        const field = this.formContext.getAttribute(fieldName);

        if (field == null) {
            return;
        }

        field.controls.forEach(function (c, i) {
            c.setVisible(visible);
        });
    }

    this.setFieldVisibility = function(fieldName, visible) {
        const control = this.formContext.getControl(fieldName);

        if (control == null) {
            return;
        }

        control.setVisible(visible);
    }

    this.showField = function (fieldName) {
        const control = this.formContext.getControl(fieldName);

        if (control == null) {
            return;
        }

        control.setVisible(true);
    }

    this.hideField = function (fieldName) {
        const control = this.formContext.getControl(fieldName);

        if (control == null) {
            return;
        }

        control.setVisible(false);
    }

    this.setFieldRequired = function (fieldName) {
        const attribute = this.formContext.getAttribute(fieldName);

        if (attribute == null) {
            return;
        }

        attribute.setRequiredLevel("required");
    }

    this.setFieldNotRequired = function (fieldName) {
        const attribute = this.formContext.getAttribute(fieldName);

        if (attribute == null) {
            return;
        }

        attribute.setRequiredLevel("none");
    }

    this.setFieldIsRequired = function (fieldName, required) {
        const attribute = this.formContext.getAttribute(fieldName);

        if (attribute == null) {
            return;
        }

        const requiredValue = required ? "required" : "none";

        attribute.setRequiredLevel(requiredValue);
    }

    this.setFieldIsReadOnly = function (fieldName, readOnly) {
        const control = this.formContext.getControl(fieldName);

        if (control == null) {
            return;
        }

        control.setDisabled(readOnly);
    }
    
    this.setFieldReadOnly = function (fieldName) {
        const control = this.formContext.getControl(fieldName);

        if (control == null) {
            return;
        }

        control.setDisabled(true);
    }

    this.setFieldReadWrite = function (fieldName) {
        const control = this.formContext.getControl(fieldName);

        if (control == null) {
            return;
        }

        control.setDisabled(false);
    }

    this.setFieldLabel = function (fieldName, label) {
        const control = this.formContext.getControl(fieldName);

        if (control == null) {
            return;
        }

        control.setLabel(label);
    }

    //#endregion
}).call(DoorUi);