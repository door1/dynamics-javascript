﻿/// <reference path="door_common_ui.js" />

function onLoad(executionContext) {
    const formContext = executionContext.getFormContext();

    DoorUi.initialize(executionContext);

    formContext.getAttribute("customertypecode").fireOnChange();
}

function customerTypeCode_onChange(executionContext) {
    const formContext = executionContext.getFormContext();

    const contactTypes = {
        Customer: 536260000,
        LostCustomer: 100000001,
        PotentialCustomer: 100000004,
        WonCustomer: 100000000,
        BoardMember: 100000003,
        Contractor: 1,
        Employee: 100000002,
        Vendor: 100000005
    };

    var showCompanyInfo = false;
    var companyNameLabel = "Household";

    const contactType = formContext.getAttribute("customertypecode").getValue();

    switch (contactType) {
        case contactTypes.BoardMember:
        case contactTypes.Contractor:
        case contactTypes.Employee:
        case contactTypes.Vendor:
            companyNameLabel = "Company Name";
            showCompanyInfo = true;
            break;
        default:
            break;
    }

    DoorUi.setFieldLabel("parentcustomerid", companyNameLabel);

    DoorUi.setFieldVisibility("jobtitle", showCompanyInfo);
}