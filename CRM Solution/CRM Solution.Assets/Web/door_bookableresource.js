﻿function ResourceTypeContact(executionContext) {
    var formContext = executionContext.getFormContext();

    if (formContext.ui.getFormType === 1) {
        formContext.getAttribute("resourcetype").SetValue(2);
    }
}