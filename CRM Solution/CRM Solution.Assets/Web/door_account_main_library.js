﻿/// <reference path="door_common_ui.js" />

function onLoad(executionContext) {
    const formContext = executionContext.getFormContext();

    DoorUi.initialize(executionContext);

    formContext.getAttribute("customertypecode").fireOnChange();
}

function accountType_onChange(executionContext) {
    const formContext = executionContext.getFormContext();

    const accountType = formContext.getAttribute("customertypecode").getValue();

    const builder = 3;
    const vendor = 11;
    const other = 12;
    const household = 13;

    DoorUi.setSectionVisibility("SUMMARY_TAB", "SUMMARY_TAB_section_partners", accountType !== household);

    DoorUi.setFieldVisibility("originatingleadid", accountType === household);
}
