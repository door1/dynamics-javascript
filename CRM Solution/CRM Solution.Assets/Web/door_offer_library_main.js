﻿/// <reference path="door_common_ui.js" />

function form_onLoad(executionContext) {
    const formContext = executionContext.getFormContext();

    DoorUi.initialize(executionContext);

    formContext.getAttribute("door_buyerorseller").fireOnChange();
    formContext.getAttribute("door_offerstatus").fireOnChange();
    formContext.getAttribute("door_offerfinancingtype").fireOnChange();
    formContext.getAttribute("door_contigent").fireOnChange();
}

function offerFinancingType_onChange(executionContext) {
    const formContext = executionContext.getFormContext();

    const cash = 100000000;
    const buyer = 100000000;
    const seller = 100000001;

    const financingType = formContext.getAttribute("door_offerfinancingtype").getValue();
    const offerType = formContext.getAttribute("door_buyerorseller").getValue();

    //if (financingType == null) {
    //    return;
    //}

    const showFinanceFields = financingType !== cash && financingType != null;

    DoorUi.setFieldVisibility("door_downpayment", showFinanceFields);
    DoorUi.setFieldVisibility("door_financeamount", showFinanceFields);
    DoorUi.setFieldVisibility("door_fieldcontingency", showFinanceFields);
    DoorUi.setFieldVisibility("door_financingcontingencydue", showFinanceFields);

    DoorUi.setFieldIsRequired("door_financeamount", offerType !== buyer && showFinanceFields);
    DoorUi.setFieldIsRequired("door_fieldcontingency", offerType === seller && showFinanceFields);
    DoorUi.setFieldIsRequired("door_financingcontingencydue", showFinanceFields);
}

function offerStatus_onChange(executionContext) {
    var formContext = executionContext.getFormContext();

    const offerType = formContext.getAttribute("door_buyerorseller").getValue();
    const offerStatus = formContext.getAttribute("door_offerstatus").getValue();

    const buyer = 100000000;
    const negotiation = 100000000;
    const accepted = 100000001;
    const rejected = 100000002;
    const termination = 100000005;
    const withdrawn = 100000004;
    const inProgress = 100000003;

    addBuyerRepAgreementExpirationDateNotification(formContext, offerStatus);
    
    // Accepted ========================================================================
    DoorUi.setFieldIsRequired("door_optionperiodenddate", offerStatus === accepted);
    DoorUi.setFieldVisibility("door_financingcontingencydue", offerStatus === accepted);
    DoorUi.setFieldVisibility("door_executedcontractdate", offerStatus === accepted);
    DoorUi.setFieldIsRequired("door_escrowofficername", offerStatus === accepted && offerType !== buyer);
    DoorUi.setFieldIsRequired("door_escrowofficeremail", offerStatus === accepted && offerType !== buyer);
    DoorUi.setFieldIsRequired("door_escrowofficephone", offerStatus === accepted && offerType !== buyer);

    if (offerStatus === accepted) {
        if (formContext.getAttribute("door_titlecompany").getValue() === "Door Title") {
            formContext.getAttribute("door_titleescrowfee2").setValue(500.00);
        }
    }

    // In Progress ======================================================================
    DoorUi.setFieldIsRequired("door_street1", offerStatus !== inProgress && offerType === buyer);
    DoorUi.setFieldIsRequired("door_city", offerStatus !== inProgress && offerType === buyer);
    DoorUi.setFieldIsRequired("door_city", offerStatus !== inProgress && offerType === buyer);
    DoorUi.setFieldIsRequired("door_stateorprovince", offerStatus !== inProgress && offerType === buyer);
    DoorUi.setFieldIsRequired("door_zippostalcode", offerStatus !== inProgress && offerType === buyer);
    DoorUi.setFieldIsRequired("door_county", offerStatus !== inProgress && offerType === buyer);

    // Termination =====================================================================
    DoorUi.setFieldVisibility("door_terminationreason", offerStatus === termination);
    DoorUi.setFieldVisibility("door_terminateddate", offerStatus === termination);
    DoorUi.setFieldIsRequired("door_terminationreason", offerStatus === termination);
    DoorUi.setFieldIsRequired("door_terminateddate", offerStatus === termination);

    // Rejected ========================================================================
    DoorUi.setFieldVisibility("door_rejectionreason", offerStatus === rejected);
    DoorUi.setFieldIsRequired("door_rejectionreason", offerStatus === rejected);

    // Negotiation =====================================================================
    if (offerStatus === negotiation) {
        if (offerType === buyer && !checkAddressFields(formContext)) {
            const alertStrings = { text: "The address fields must but completed before progressing to the Negotiate state.", title: "Notice" };

            Xrm.Navigation.openAlertDialog(alertStrings).then(
                function (result) {
                    const initialOfferStatus = formContext.getAttribute("door_offerstatus").getInitialValue();

                    formContext.getAttribute("door_offerstatus").setValue(initialOfferStatus);
                    formContext.getControl("door_street1").setFocus();
                },
                function (error) {
                    console.log(error.message);
                }
            );
        }
    }
}

function addBuyerRepAgreementExpirationDateNotification(formContext, offerStatus) {
    const inProgress = 100000003;

    const repAgreementDate = formContext.getAttribute("door_buyerrepagreementdate").getValue();
    const repAgreementExpirationDate = formContext.getAttribute("door_buyerrepagreementexpirationdate").getValue();

    const repAgreementDateControl = formContext.getControl("door_buyerrepagreementdate");
    const repAgreementExpirationDateControl = formContext.getControl("door_buyerrepagreementexpirationdate");

    if (offerStatus === inProgress) {
        repAgreementDateControl.clearNotification("buyerRepAgreementDate");
        repAgreementExpirationDateControl.clearNotification("buyerRepAgreementExpirationDate");

        return;
    }

    if (repAgreementExpirationDate == null) {
        repAgreementExpirationDateControl.setNotification(
            "Buyer rep agreement information is required on the opportunity.",
            "buyerRepAgreementExpirationDate");
    } else {
        repAgreementExpirationDateControl.clearNotification("buyerRepAgreementExpirationDate");
    }

    if (repAgreementDate == null) {
        repAgreementDateControl.setNotification(
            "Buyer rep agreement information is required on the opportunity.",
            "buyerRepAgreementDate");
    } else {
        repAgreementDateControl.clearNotification("buyerRepAgreementDate");
    }
}

function optionPeriodFee_onChange(executionContext) {
    const formContext = executionContext.getFormContext();

    const optionPeriodFee = formContext.getAttribute("door_optionperiodfee").getValue();

    formContext.getAttribute("door_optionmoney").setValue(optionPeriodFee);
}

function isDoorTitle_onChange(executionContext) {
    const formContext = executionContext.getFormContext();

    const isDoorTitle = formContext.getAttribute("door_isdoortitle").getValue();

    const yes = 100000000;
    const doorTitlePhone = "(469) 480-4387";
    const doorTitleCompany = "Door Title";

    const phone = isDoorTitle === yes ? doorTitlePhone : "";
    const company = isDoorTitle === yes ? doorTitleCompany : "";

    formContext.getAttribute("door_escrowofficephone").setValue(phone);
    formContext.getAttribute("door_titlecompany").setValue(company);
}

function contigent_onChange(executionContext) {
    const formContext = executionContext.getFormContext();

    const contigent = formContext.getAttribute("door_contigent").getValue();

    DoorUi.setFieldVisibility("door_contingencyenddate", contigent);
}

function fieldcontingency_onChange(executionContext) {
    const formContext = executionContext.getFormContext();

    const executedContractDate = formContext.getAttribute("door_executedcontractdate").getValue();
    const fieldContingencyLength = formContext.getAttribute("door_fieldcontingency").getValue();

    if (executedContractDate == null) {
        return;
    }

    const financingContingencyDue = addDays(executedContractDate, fieldContingencyLength);

    formContext.getAttribute("door_financingcontingencydue").setValue(financingContingencyDue);
}

function calculateDueDiligenceDate(executionContext) {
    const formContext = executionContext.getFormContext();

    const executedContractDate = formContext.getAttribute("door_executedcontractdate").getValue();
    var optionPeriodLength = formContext.getAttribute("door_optionperiodlength").getValue();

    if (executedContractDate == null) {
        return;
    }

    //
    // The door_optionperiodlength field is a string, so we have to convert it to an int
    //
    if (optionPeriodLength == null) {
        optionPeriodLength = 0;
    }

    const optionPeriodEndDate = addDays(executedContractDate, parseInt(optionPeriodLength, 10));

    optionPeriodEndDate.setHours(17);   // 5:00pm

    formContext.getAttribute("door_optionperiodenddate").setValue(optionPeriodEndDate);
}

function addDays(date, days) {
    const result = new Date(date);

    result.setDate(result.getDate() + days);

    return result;
}

function processBuyerSeller(executionContext) {
    const formContext = executionContext.getFormContext();

    const leadType = formContext.getAttribute("door_buyerorseller").getValue();

    const buyer = 100000000;
    const seller = 100000001;

    DoorUi.setTabVisibility("tab_buyer", leadType === buyer);
    DoorUi.setTabVisibility("tab_seller", leadType === seller);
    DoorUi.setTabVisibility("tab_timeline", leadType === seller);

    DoorUi.setSectionVisibility("tab_general", "tab_general_section_net_sheet", leadType === seller);

    DoorUi.setFieldVisibility("door_doorfee", leadType === seller);
    DoorUi.setFieldVisibility("door_offerexpirationdate", leadType === seller);
    DoorUi.setFieldVisibility("door_buyerrepagreementdate", leadType === buyer);
    DoorUi.setFieldVisibility("door_buyerrepagreementexpirationdate", leadType === buyer);
    DoorUi.setFieldVisibility("door_sendoffernotification", leadType !== buyer);

    DoorUi.setFieldIsRequired("door_financingcontingencydue", leadType !== buyer);
    DoorUi.setFieldIsRequired("door_financeamount", leadType === seller);
    DoorUi.setFieldIsRequired("door_fieldcontingency", leadType === seller);

    formContext.getAttribute("door_offerfinancingtype").fireOnChange();
}

function checkAddressFields(formContext) {
    const street1 = formContext.getAttribute("door_street1").getValue();
    //const street2 = formContext.getAttribute("door_street2").getValue();
    const city = formContext.getAttribute("door_city").getValue();
    const state = formContext.getAttribute("door_stateorprovince").getValue();
    const zip = formContext.getAttribute("door_zippostalcode").getValue();
    const county = formContext.getAttribute("door_county").getValue();

    if (street1 == null || /*street2 == null || */ city == null || state == null || zip == null || county == null) {
        return false;
    }

    return true;
}
