﻿/// <reference path="door_common.js" />
/// <reference path="intellisensefiles/xrm.d.ts" />

if (typeof door === "undefined") {
    door = {
        __namespace: true
    };
}

door.opportunity = (function () {
    var CONSTANTS = {
        parentContactIdFieldName: "parentcontactid",
        doorOpportunityTypeFieldName: "door_opportunitytype1",
        nameFieldName: "name",
        separatorString: " - "
    };

    function onLoad(executionContext) {
        console.log("in onload");

        initialize(executionContext);
        updateName(executionContext);
    };

    function initialize(executionContext) {
        console.log("in initialize");

        bindOnChangeEvents(executionContext);
    };

    function bindOnChangeEvents(executionContext) {
        console.log("in bindOnChangeEvents");

        var formContext = executionContext.getFormContext();

        door.common.bindEventChangeHandler(formContext, CONSTANTS.doorOpportunityTypeFieldName, updateName);
        door.common.bindEventChangeHandler(formContext, CONSTANTS.parentContactIdFieldName, updateName);
    };

    //OnLoad & OnChange of Contact or Opportunity Type
    //Set name = { parentcontactid } - { door_opportunitytype }
    function updateName(executionContext) {
        console.log("in updateName");

        var formContext = executionContext.getFormContext();

        var oppType = door.common.getText(formContext, CONSTANTS.doorOpportunityTypeFieldName);
        var parentContactId = door.common.getText(formContext, CONSTANTS.parentContactIdFieldName);

        console.log("oppType: " + oppType);
        console.log("parentContactId: " + parentContactId);

        var newName = oppType + CONSTANTS.separatorString + parentContactId;

        console.log("NewName : " + newName);

        formContext.getAttribute(CONSTANTS.nameFieldName).setValue(newName);
    };

    return {
        onLoad: onLoad
    };
})();

if (window.parent) {
    window.parent.door = door;
}