﻿if (typeof (door) == "undefined") {
    door = {
        __namespace: true
    };
}

door.common = (function () {
    function getDayOfTheWeek(day) {
        const weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

        return weekday[day.getDay()];
    }

    function bindEventChangeHandler(formContext, fieldName, onChangeHandler) {
        var attribute = formContext.getAttribute(fieldName);

        if (!attribute) {
            return;
        }

        attribute.addOnChange(onChangeHandler);
    }

    function setValue(formContext, fieldName, fieldValue) {
        var attribute = formContext.getAttribute(fieldName);
        var value;

        if (attribute != null) {
            if (attribute.getAttributeType() === "lookup") {
                value = getArrayValue(fieldValue);
            } else {
                value = fieldValue;
            }

            attribute.setValue(value);
        }
    }

    function getValue(formContext, fieldName) {
        var attribute = formContext.getAttribute(fieldName);

        if (attribute != null) {
            return attribute.getValue(fieldName);
        }

        return "";
    }

    function getText(formContext, fieldName) {
        const attribute = formContext.getAttribute(fieldName);

        if (attribute === null || attribute.getValue() === null) {
            return "";
        }

        if (attribute.getAttributeType() === "lookup") {
            return attribute.getValue()[0].name;
        }

        if (attribute.getAttributeType() === "optionset" ||
            attribute.getAttributeType() === "multiselectoptionset") {
            return attribute.getText();
        }

        if (attribute.getAttributeType() === "string" ||
            attribute.getAttributeType() === "memo") {
            return attribute.getValue();
        }

        return attribute.getValue().toString();
    }

    //taken from SDK -- not used
    //function IsArray(value) {
    //    return !!value && ((Array.isInstanceOfType(value)) || (typeof (value.splice)) === 'function');
    //}

    function setReadOnly(formContext, fieldName, readOnly) {
        var control = formContext.getControl(fieldName);

        if (!control) {
            return;
        }

        control.setDisabled(readOnly);
    }

    function setVisibility(formContext, fieldName, visible) {
        var control = formContext.getControl(fieldName);

        if (!control) {
            return;
        }

        control.setVisible(visible);
    }

    function showTab(formContext, tabName) {
        formContext.ui.tabs.get(tabName).setVisible(true);
    }

    function hideTab(formContext, tabName) {
        formContext.ui.tabs.get(tabName).setVisible(false);
    }

    function showSection(formContext, tabName, sectionName) {
        formContext.ui.tabs.get(tabName).sections.get(sectionName).setVisible(true);
    }

    function hideSection(formContext, tabName, sectionName) {
        formContext.ui.tabs.get(tabName).sections.get(sectionName).setVisible(false);
    }

    function getOptions(formContext, fieldName) {
        var attribute = formContext.getAttribute(fieldName);

        if (!attribute) {
            return null;
        }

        return attribute.getOptions();
    }

    function getOption(formContext, fieldName, optionValue) {
        var attribute = formContext.getAttribute(fieldName);

        if (!attribute) {
            return null;
        }

        return attribute.getOption(optionValue);
    }

    function lookupFieldHasValue(formContext, fieldName) {
        var returnedValue = formContext.getAttribute(fieldName).getValue(fieldName);

        return (returnedValue && returnedValue.length > 0);
    }

    function isOptionEqual(formContext, fieldName, optionValue, testText) {
        var control = formContext.getControl(fieldName);
        var option = control.getOption(fieldName, optionValue);

        if (option) {
            return option.text.toLowerCase() === testText.toLowerCase();
        } else {
            return false;
        }
    }

    function getArrayValue(fieldValueObject) {
        if (!fieldValueObject) {
            return null;
        }

        var lookupValue = [
            {
                id: fieldValueObject.Id,
                entityType: fieldValueObject.LogicalName,
                name: fieldValueObject.Name
            }];

        return lookupValue;
    }

    return {
        getValue: getValue,
        getText: getText,
        setValue: setValue,
        getDayOfTheWeek: getDayOfTheWeek,
        bindEventChangeHandler: bindEventChangeHandler,
        setVisibility: setVisibility,
        setReadOnly: setReadOnly,
        getOption: getOption,
        getOptions: getOptions,
        isOptionEqual: isOptionEqual,
        lookupFieldHasValue: lookupFieldHasValue
    }
})();

if (window.parent) window.parent.door = door;