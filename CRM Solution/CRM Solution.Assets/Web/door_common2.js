﻿var DoorSecurity = window.DoorSecurity || {};

(function () {
    this.securityRoleList = [];

    this.initialize = async function (executionContext) {
        //var formContext = executionContext.getFormContext();

        await this.getUserSecurityRoles();
    }

    this.getUserSecurityRoles = async function () {
        var self = this;

        const queryBuilder = ["?$select=name&$filter="];

        const securityRoles = Xrm.Utility.getGlobalContext().userSettings.securityRoles;

        for (let i = 0; i < securityRoles.length; i++) {
            queryBuilder.push("roleid eq ");
            queryBuilder.push(securityRoles[i]);
            queryBuilder.push(" or ");
        }

        // Remove the extra " or " clause
        queryBuilder.splice(queryBuilder.length - 1, 1);

        await Xrm.WebApi.online.retrieveMultipleRecords("role", queryBuilder.join("")).then(
        function success(results) {
            for (let i = 0; i < results.entities.length; i++) {
                self.securityRoleList.push(results.entities[i]["name"]);
            }
        },
        function (error) {
            Xrm.Navigation.openAlertDialog({
                text: error.message
            });
        });
    }

    this.userHasRole = function (roleName) {
        if (Array.prototype.indexOf) {
            return this.securityRoleList.indexOf(roleName) > -1;
        }

        return false;
    }
}).call(DoorSecurity);
