﻿function DisplayCustomIcons(row, lcid) {
    var tooltip = "";
    var imageName = "";
    var rowStr = JSON.parse(row);
    var columnData = rowStr.statuscode;

    switch (columnData) {
        case 1:
        case "Open":
            imageName = "door_open16.png";
            tooltip = "Open";
            break;
        case "Requested":
            imageName = "door_requested16.png";
            tooltip = "Requested";
            break;
        case "Scheduled":
            imageName = "door_scheduled16.png";
            tooltip = "Scheduled";
            break;
        case "Completed":
            imageName = "door_completed16.png";
            tooltip = "Completed";
            break;
        case "Cancelled":
            imageName = "door_cancelled16.png";
            tooltip = "Cancelled";
            break;
        case "Draft":
            imageName = "door_draft16.png";
            tooltip = "Draft";
            break;
        case "Closed":
            imageName = "door_closed16.svg";
            tooltip = "Closed";
            break;
        default:
            break;
    }

    return [imageName, tooltip];
}

function dateOnChange(executionContext) {
    var formContext = executionContext.getFormContext();

    var startTime = formContext.getAttribute("door_scheduledstart").getValue();

    if (startTime == null) {
        return;
    }

    //  var newTime = new Date();
    //  newTime.setHours(newTime.getHours() + 2);

    //advance time needed
    var advancedTime = new Date();
    advancedTime.setHours(advancedTime.getHours() + 2);

    if (startTime <= advancedTime) {
        formContext.getAttribute("door_scheduledstart").setValue(advancedTime);

        //Xrm.Page.getControl("door_scheduledstart").setNotification("Jobs can only be scheduled 2 hours in advance of the scheduled start time.");
        //Xrm.Page.ui.setFormNotification("Jobs can only be scheduled 2 hours in advance of the scheduled start time.","WARNING")
        //alert ("Jobs can only be scheduled 2 hours in advance of the scheduled start time.");
        // clean the field
    }
}