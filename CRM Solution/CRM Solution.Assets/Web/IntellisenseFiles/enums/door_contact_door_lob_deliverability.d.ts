﻿declare const enum door_contact_door_lob_deliverability {
    Deliverable = 100000000,
    Undeliverable = 100000001,
    Incomplete = 100000002,
}
