﻿declare const enum need {
    MustHave = 0,
    ShouldHave = 1,
    GoodToHave = 2,
    NoNeed = 3,
}
