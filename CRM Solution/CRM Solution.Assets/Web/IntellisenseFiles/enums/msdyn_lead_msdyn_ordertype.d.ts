﻿declare const enum msdyn_lead_msdyn_ordertype {
    WorkBased = 192350001,
    ItemBased = 192350000,
    Service_MaintenanceBased = 690970002,
    Buyer = 536260000,
    Seller = 536260001,
}
