﻿declare const enum msdyn_leadscoretrendoptset {
    Improving = 0,
    Steady = 1,
    Declining = 2,
    NotEnoughInfo = 3,
}
