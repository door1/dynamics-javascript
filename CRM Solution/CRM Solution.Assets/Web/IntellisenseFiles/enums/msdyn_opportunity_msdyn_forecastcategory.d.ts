﻿declare const enum msdyn_opportunity_msdyn_forecastcategory {
    Pipeline_lowConfidence = 100000001,
    BestCase_moderateConfidence = 100000002,
    Committed_highConfidence = 100000003,
    Omitted_excludeFromForecast = 100000004,
}
