﻿declare const enum door_opportunity_door_signinyardpreference {
    NoSign = 100000000,
    Now = 100000001,
    DayOfAppointment = 100000002,
    DayOfListingGoingOnMarket = 100000003,
    NoPreference = 100000004,
}
