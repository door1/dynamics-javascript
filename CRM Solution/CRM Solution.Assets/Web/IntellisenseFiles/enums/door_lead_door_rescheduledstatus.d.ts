﻿declare const enum door_lead_door_rescheduledstatus {
    NoShow = 536260000,
    ActivelyRescheduling = 536260001,
    Rescheduled = 536260002,
}
