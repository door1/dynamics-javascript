﻿declare const enum door_opportunity_door_methodofvendorentry {
    ComboBox = 100000000,
    KeyPad = 100000001,
    SellerWillBePresent = 100000002,
    SpareKeyHidden = 100000003,
    OtherArrangementsNeedToBeMade = 100000004,
}
