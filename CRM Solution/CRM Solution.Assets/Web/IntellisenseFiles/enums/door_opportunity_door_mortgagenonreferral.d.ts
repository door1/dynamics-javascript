﻿declare const enum door_opportunity_door_mortgagenonreferral {
    _N_A = 536260000,
    ProductNotAvailable = 100000000,
    DoorCouldntQualify = 100000001,
    OtherLenderBeatRates = 100000002,
    CashBuyer = 100000003,
    FamilyLender = 100000004,
    LenderAlreadyAttached = 100000005,
    BuyerRefused = 100000006,
    ThirdPartyBuyerRefused = 100000007,
}
