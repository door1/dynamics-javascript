﻿declare const enum door_opportunity_door_buyerprequalifiedstatus {
    NotQualified = 536260000,
    CashBuyer = 536260001,
    Yes_OtherLender = 536260002,
    Yes = 536260003,
    Yes_DoorMortgage = 536260004,
}
