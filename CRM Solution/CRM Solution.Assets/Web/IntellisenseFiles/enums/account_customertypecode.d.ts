﻿declare const enum account_customertypecode {
    Vendor = 11,
    Builder = 3,
    Household = 13,
    Other = 12,
}
