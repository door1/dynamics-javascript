﻿declare const enum door_lead_door_whendoyouneedtobeoutofthehome {
    Immediately = 536260000,
    NextMonth = 536260001,
    TwoMonths = 536260002,
    SixMonths = 536260003,
    OneYear = 536260004,
}
