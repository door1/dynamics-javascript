﻿declare const enum door_opportunity_door_portalflowcontrol {
    UnlockOnboarding = 100000000,
    UnlockScheduleConfirmation = 100000001,
    UnlockListingIsLive = 100000002,
    UnlockContractToClosing = 100000003,
}
