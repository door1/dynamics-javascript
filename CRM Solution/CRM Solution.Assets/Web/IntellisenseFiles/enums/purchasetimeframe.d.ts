﻿declare const enum purchasetimeframe {
    Immediate = 0,
    Next2Months = 100000000,
    MoreThan2Months = 100000001,
    ThisQuarter = 1,
    NextQuarter = 2,
    ThisYear = 3,
    NextYear = 100000002,
    Unknown = 4,
}
