﻿declare const enum msdyn_opportunityscoretrendoptset {
    Improving = 0,
    Steady = 1,
    Declining = 2,
    NotEnoughInfo = 3,
}
