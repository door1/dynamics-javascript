﻿declare const enum door_lead_door_kitchencountertops {
    Quartz = 536260000,
    Formica = 536260001,
    Granite = 536260002,
    Marble = 536260003,
    Butcher_Block = 536260004,
    Concrete = 536260005,
    Metal = 536260006,
    PatternedStone = 536260007,
    MixedMaterials = 536260008,
    Other = 536260009,
}
