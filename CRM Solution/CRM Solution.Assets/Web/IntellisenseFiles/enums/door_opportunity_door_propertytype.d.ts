﻿declare const enum door_opportunity_door_propertytype {
    SingleFamily = 100000000,
    Condominium = 100000001,
    Land = 100000002,
    Multiunit = 100000003,
    Other = 100000004,
    SingleFamilyHome = 536260000,
    Condo = 536260001,
    Multi_Unit = 536260002,
    TownHome = 536260003,
}
