﻿declare const enum door_opportunity_door_timelinetomove {
    Immediately = 536260000,
    NextMonth = 536260001,
    TwoMonths = 536260002,
    SixMonths = 536260003,
    OneYear = 536260004,
}
