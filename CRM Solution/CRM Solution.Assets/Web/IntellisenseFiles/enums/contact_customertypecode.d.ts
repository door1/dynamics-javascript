﻿declare const enum contact_customertypecode {
    BoardMember = 100000003,
    Contractor = 1,
    Employee = 100000002,
    LostCustomer = 100000001,
    PotentialCustomer = 100000004,
    Vendor = 100000005,
    WonCustomer = 100000000,
}
