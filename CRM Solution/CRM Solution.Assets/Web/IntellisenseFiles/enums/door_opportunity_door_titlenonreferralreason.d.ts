﻿declare const enum door_opportunity_door_titlenonreferralreason {
    _N_A = 536260000,
    ExistingTitleDispute = 536260001,
    RelocationCompay = 536260002,
    BuyerPaid = 100000000,
    BuyerPreference = 100000001,
    AgentRequirement = 100000002,
    SellerRefused = 100000003,
    SellerRequestedOther = 100000004,
    LenderRefusal = 100000005,
    ExisitingTitleDispute = 100000006,
    RelocationCompany = 100000007,
}
