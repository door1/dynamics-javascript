﻿declare const enum door_addressdeliverability {
    Deliverable = 100000000,
    Undeliverable = 100000001,
    Incomplete = 100000002,
}
