﻿declare namespace Form.account.Main {
    namespace Account {
        namespace Tabs {
            interface SUMMARYTAB extends XrmBase.SectionCollectionBase {
                get(name: "ACCOUNT_INFORMATION"): XrmBase.PageSection;
                get(name: "SUMMARY_TAB_section_partners"): XrmBase.PageSection;
                get(name: "MapSection"): XrmBase.PageSection;
                get(name: "ADDRESS"): XrmBase.PageSection;
                get(name: "SOCIAL_PANE_TAB"): XrmBase.PageSection;
                get(name: "SUMMARY_TAB_section_6"): XrmBase.PageSection;
                get(name: "NEWS_SECTION"): XrmBase.PageSection;
                get(name: "Summary_section_6"): XrmBase.PageSection;
                get(name: string): undefined;
                get(): XrmBase.PageSection[];
                get(index: number): XrmBase.PageSection;
                get(chooser: (item: XrmBase.PageSection, index: number) => boolean): XrmBase.PageSection[];
            }
            interface Tab4 extends XrmBase.SectionCollectionBase {
                get(name: "tab_4_section_1"): XrmBase.PageSection;
                get(name: string): undefined;
                get(): XrmBase.PageSection[];
                get(index: number): XrmBase.PageSection;
                get(chooser: (item: XrmBase.PageSection, index: number) => boolean): XrmBase.PageSection[];
            }
            interface Tab7 extends XrmBase.SectionCollectionBase {
                get(name: "tab_7_section_1"): XrmBase.PageSection;
                get(name: string): undefined;
                get(): XrmBase.PageSection[];
                get(index: number): XrmBase.PageSection;
                get(chooser: (item: XrmBase.PageSection, index: number) => boolean): XrmBase.PageSection[];
            }
            interface Tab6 extends XrmBase.SectionCollectionBase {
                get(name: "tab_6_section_1"): XrmBase.PageSection;
                get(name: string): undefined;
                get(): XrmBase.PageSection[];
                get(index: number): XrmBase.PageSection;
                get(chooser: (item: XrmBase.PageSection, index: number) => boolean): XrmBase.PageSection[];
            }
            interface Tab8 extends XrmBase.SectionCollectionBase {
                get(name: "tab_8_section_1"): XrmBase.PageSection;
                get(name: string): undefined;
                get(): XrmBase.PageSection[];
                get(index: number): XrmBase.PageSection;
                get(chooser: (item: XrmBase.PageSection, index: number) => boolean): XrmBase.PageSection[];
            }
            interface Tab10 extends XrmBase.SectionCollectionBase {
                get(name: "tab_10_section_1"): XrmBase.PageSection;
                get(name: string): undefined;
                get(): XrmBase.PageSection[];
                get(index: number): XrmBase.PageSection;
                get(chooser: (item: XrmBase.PageSection, index: number) => boolean): XrmBase.PageSection[];
            }
            interface Documentssharepoint extends XrmBase.SectionCollectionBase {
                get(name: "documents_sharepoint_section"): XrmBase.PageSection;
                get(name: string): undefined;
                get(): XrmBase.PageSection[];
                get(index: number): XrmBase.PageSection;
                get(chooser: (item: XrmBase.PageSection, index: number) => boolean): XrmBase.PageSection[];
            }
            interface Linkedinv2tab extends XrmBase.SectionCollectionBase {
                get(name: "linkedin_v2_tab_section_1"): XrmBase.PageSection;
                get(name: "linkedin_v2_tab_section_2"): XrmBase.PageSection;
                get(name: string): undefined;
                get(): XrmBase.PageSection[];
                get(index: number): XrmBase.PageSection;
                get(chooser: (item: XrmBase.PageSection, index: number) => boolean): XrmBase.PageSection[];
            }
        }
        interface Attributes extends XrmBase.AttributeCollectionBase {
            get(name: "accountnumber"): XrmBase.StringAttribute;
            get(name: "address1_city"): XrmBase.StringAttribute;
            get(name: "address1_country"): XrmBase.StringAttribute;
            get(name: "address1_county"): XrmBase.StringAttribute;
            get(name: "address1_line1"): XrmBase.StringAttribute;
            get(name: "address1_line2"): XrmBase.StringAttribute;
            get(name: "address1_postalcode"): XrmBase.StringAttribute;
            get(name: "address1_stateorprovince"): XrmBase.StringAttribute;
            get(name: "customertypecode"): XrmBase.OptionSetAttribute<account_customertypecode>;
            get(name: "fax"): XrmBase.StringAttribute;
            get(name: "ms_newsandsocialid"): XrmBase.StringAttribute;
            get(name: "ms_traceid"): XrmBase.StringAttribute;
            get(name: "name"): XrmBase.StringAttribute;
            get(name: "parentaccountid"): XrmBase.LookupAttribute<"account">;
            get(name: "primarycontactid"): XrmBase.LookupAttribute<"contact">;
            get(name: "telephone1"): XrmBase.StringAttribute;
            get(name: "websiteurl"): XrmBase.StringAttribute;
            get(name: string): undefined;
            get(): XrmBase.Attribute<any>[];
            get(index: number): XrmBase.Attribute<any>;
            get(chooser: (item: XrmBase.Attribute<any>, index: number) => boolean): XrmBase.Attribute<any>[];
        }
        interface Controls extends XrmBase.ControlCollectionBase {
            get(name: "accountnumber"): XrmBase.StringControl;
            get(name: "accounts"): XrmBase.SubGridControl<"account">;
            get(name: "ActionCards"): XrmBase.BaseControl;
            get(name: "address1_city"): XrmBase.StringControl;
            get(name: "address1_country"): XrmBase.StringControl;
            get(name: "address1_county"): XrmBase.StringControl;
            get(name: "address1_line1"): XrmBase.StringControl;
            get(name: "address1_line2"): XrmBase.StringControl;
            get(name: "address1_postalcode"): XrmBase.StringControl;
            get(name: "address1_stateorprovince"): XrmBase.StringControl;
            get(name: "contactquickform"): XrmBase.BaseControl;
            get(name: "contacts"): XrmBase.SubGridControl<"contact">;
            get(name: "Contacts"): XrmBase.SubGridControl<"contact">;
            get(name: "customertypecode"): XrmBase.OptionSetControl<account_customertypecode>;
            get(name: "documentlocations"): XrmBase.SubGridControl<"sharepointdocumentlocation">;
            get(name: "DocumentsSubGrid"): XrmBase.SubGridControl<"sharepointdocument">;
            get(name: "fax"): XrmBase.StringControl;
            get(name: "leads"): XrmBase.SubGridControl<"lead">;
            get(name: "mapcontrol"): XrmBase.BaseControl;
            get(name: "ms_newsandsocialid"): XrmBase.StringControl;
            get(name: "ms_traceid"): XrmBase.StringControl;
            get(name: "name"): XrmBase.StringControl;
            get(name: "News"): XrmBase.BaseControl;
            get(name: "notescontrol"): XrmBase.StringControl;
            get(name: "opportunity"): XrmBase.SubGridControl<"opportunity">;
            get(name: "parentaccountid"): XrmBase.LookupControl<"account">;
            get(name: "primarycontactid"): XrmBase.LookupControl<"contact">;
            get(name: "telephone1"): XrmBase.StringControl;
            get(name: "websiteurl"): XrmBase.StringControl;
            get(name: string): undefined;
            get(): XrmBase.BaseControl[];
            get(index: number): XrmBase.BaseControl;
            get(chooser: (item: XrmBase.BaseControl, index: number) => boolean): XrmBase.BaseControl[];
        }
        interface Tabs extends XrmBase.TabCollectionBase {
            get(name: "SUMMARY_TAB"): XrmBase.PageTab<Tabs.SUMMARYTAB>;
            get(name: "tab_4"): XrmBase.PageTab<Tabs.Tab4>;
            get(name: "tab_7"): XrmBase.PageTab<Tabs.Tab7>;
            get(name: "tab_6"): XrmBase.PageTab<Tabs.Tab6>;
            get(name: "tab_8"): XrmBase.PageTab<Tabs.Tab8>;
            get(name: "tab_10"): XrmBase.PageTab<Tabs.Tab10>;
            get(name: "documents_sharepoint"): XrmBase.PageTab<Tabs.Documentssharepoint>;
            get(name: "linkedin_v2_tab"): XrmBase.PageTab<Tabs.Linkedinv2tab>;
            get(name: string): undefined;
            get(): XrmBase.PageTab<XrmBase.Collection<XrmBase.PageSection>>[];
            get(index: number): XrmBase.PageTab<XrmBase.Collection<XrmBase.PageSection>>;
            get(chooser: (item: XrmBase.PageTab<XrmBase.Collection<XrmBase.PageSection>>, index: number) => boolean): XrmBase.PageTab<XrmBase.Collection<XrmBase.PageSection>>[];
        }
    }
    interface Account extends XrmBase.PageBase<Account.Attributes, Account.Tabs, Account.Controls> {
        getAttribute(attributeName: "customertypecode"): XrmBase.OptionSetAttribute<account_customertypecode>;
        getAttribute(attributeName: "name"): XrmBase.StringAttribute;
        getAttribute(attributeName: "accountnumber"): XrmBase.StringAttribute;
        getAttribute(attributeName: "telephone1"): XrmBase.StringAttribute;
        getAttribute(attributeName: "fax"): XrmBase.StringAttribute;
        getAttribute(attributeName: "websiteurl"): XrmBase.StringAttribute;
        getAttribute(attributeName: "parentaccountid"): XrmBase.LookupAttribute<"account">;
        getAttribute(attributeName: "address1_line1"): XrmBase.StringAttribute;
        getAttribute(attributeName: "address1_line2"): XrmBase.StringAttribute;
        getAttribute(attributeName: "address1_city"): XrmBase.StringAttribute;
        getAttribute(attributeName: "address1_stateorprovince"): XrmBase.StringAttribute;
        getAttribute(attributeName: "address1_postalcode"): XrmBase.StringAttribute;
        getAttribute(attributeName: "address1_county"): XrmBase.StringAttribute;
        getAttribute(attributeName: "address1_country"): XrmBase.StringAttribute;
        getAttribute(attributeName: "primarycontactid"): XrmBase.LookupAttribute<"contact">;
        getAttribute(attributeName: "ms_newsandsocialid"): XrmBase.StringAttribute;
        getAttribute(attributeName: "ms_traceid"): XrmBase.StringAttribute;
        getAttribute(attributeName: string): undefined;
        getControl(controlName: "customertypecode"): XrmBase.OptionSetControl<account_customertypecode>;
        getControl(controlName: "name"): XrmBase.StringControl;
        getControl(controlName: "accountnumber"): XrmBase.StringControl;
        getControl(controlName: "telephone1"): XrmBase.StringControl;
        getControl(controlName: "fax"): XrmBase.StringControl;
        getControl(controlName: "websiteurl"): XrmBase.StringControl;
        getControl(controlName: "parentaccountid"): XrmBase.LookupControl<"account">;
        getControl(controlName: "mapcontrol"): XrmBase.BaseControl;
        getControl(controlName: "address1_line1"): XrmBase.StringControl;
        getControl(controlName: "address1_line2"): XrmBase.StringControl;
        getControl(controlName: "address1_city"): XrmBase.StringControl;
        getControl(controlName: "address1_stateorprovince"): XrmBase.StringControl;
        getControl(controlName: "address1_postalcode"): XrmBase.StringControl;
        getControl(controlName: "address1_county"): XrmBase.StringControl;
        getControl(controlName: "address1_country"): XrmBase.StringControl;
        getControl(controlName: "notescontrol"): XrmBase.StringControl;
        getControl(controlName: "primarycontactid"): XrmBase.LookupControl<"contact">;
        getControl(controlName: "contactquickform"): XrmBase.BaseControl;
        getControl(controlName: "Contacts"): XrmBase.SubGridControl<"contact">;
        getControl(controlName: "News"): XrmBase.BaseControl;
        getControl(controlName: "ms_newsandsocialid"): XrmBase.StringControl;
        getControl(controlName: "ms_traceid"): XrmBase.StringControl;
        getControl(controlName: "ActionCards"): XrmBase.BaseControl;
        getControl(controlName: "contacts"): XrmBase.SubGridControl<"contact">;
        getControl(controlName: "opportunity"): XrmBase.SubGridControl<"opportunity">;
        getControl(controlName: "leads"): XrmBase.SubGridControl<"lead">;
        getControl(controlName: "accounts"): XrmBase.SubGridControl<"account">;
        getControl(controlName: "documentlocations"): XrmBase.SubGridControl<"sharepointdocumentlocation">;
        getControl(controlName: "DocumentsSubGrid"): XrmBase.SubGridControl<"sharepointdocument">;
        getControl(controlName: string): undefined;
    }
}
