﻿declare namespace Form.contact.Main {
    namespace ContactExperimental {
        namespace Tabs {
            interface SUMMARYTAB extends XrmBase.SectionCollectionBase {
                get(name: "CONTACT_INFORMATION"): XrmBase.PageSection;
                get(name: "PERSONAL_NOTES_SECTION"): XrmBase.PageSection;
                get(name: "SUMMARY_TAB_section_7"): XrmBase.PageSection;
                get(name: "MapSection"): XrmBase.PageSection;
                get(name: "NEWS_SECTION"): XrmBase.PageSection;
                get(name: "TalkingPoints_section"): XrmBase.PageSection;
                get(name: "SOCIAL_PANE_TAB"): XrmBase.PageSection;
                get(name: "SUMMARY_TAB_section_9"): XrmBase.PageSection;
                get(name: string): undefined;
                get(): XrmBase.PageSection[];
                get(index: number): XrmBase.PageSection;
                get(chooser: (item: XrmBase.PageSection, index: number) => boolean): XrmBase.PageSection[];
            }
            interface Tab4 extends XrmBase.SectionCollectionBase {
                get(name: "tab_4_section_1"): XrmBase.PageSection;
                get(name: string): undefined;
                get(): XrmBase.PageSection[];
                get(index: number): XrmBase.PageSection;
                get(chooser: (item: XrmBase.PageSection, index: number) => boolean): XrmBase.PageSection[];
            }
            interface Tab5 extends XrmBase.SectionCollectionBase {
                get(name: "tab_5_section_1"): XrmBase.PageSection;
                get(name: string): undefined;
                get(): XrmBase.PageSection[];
                get(index: number): XrmBase.PageSection;
                get(chooser: (item: XrmBase.PageSection, index: number) => boolean): XrmBase.PageSection[];
            }
            interface Tab9 extends XrmBase.SectionCollectionBase {
                get(name: "tab_9_section_1"): XrmBase.PageSection;
                get(name: string): undefined;
                get(): XrmBase.PageSection[];
                get(index: number): XrmBase.PageSection;
                get(chooser: (item: XrmBase.PageSection, index: number) => boolean): XrmBase.PageSection[];
            }
            interface Tab10 extends XrmBase.SectionCollectionBase {
                get(name: "tab_10_section_1"): XrmBase.PageSection;
                get(name: string): undefined;
                get(): XrmBase.PageSection[];
                get(index: number): XrmBase.PageSection;
                get(chooser: (item: XrmBase.PageSection, index: number) => boolean): XrmBase.PageSection[];
            }
            interface Tab7 extends XrmBase.SectionCollectionBase {
                get(name: "tab_7_section_1"): XrmBase.PageSection;
                get(name: string): undefined;
                get(): XrmBase.PageSection[];
                get(index: number): XrmBase.PageSection;
                get(chooser: (item: XrmBase.PageSection, index: number) => boolean): XrmBase.PageSection[];
            }
            interface Tab8 extends XrmBase.SectionCollectionBase {
                get(name: "tab_8_section_1"): XrmBase.PageSection;
                get(name: string): undefined;
                get(): XrmBase.PageSection[];
                get(index: number): XrmBase.PageSection;
                get(chooser: (item: XrmBase.PageSection, index: number) => boolean): XrmBase.PageSection[];
            }
            interface Tab6 extends XrmBase.SectionCollectionBase {
                get(name: "SUMMARY_TAB_section_8"): XrmBase.PageSection;
                get(name: "tab_6_section_3"): XrmBase.PageSection;
                get(name: "SUMMARY_TAB_section_10"): XrmBase.PageSection;
                get(name: "tab_6_column_12_section_1"): XrmBase.PageSection;
                get(name: string): undefined;
                get(): XrmBase.PageSection[];
                get(index: number): XrmBase.PageSection;
                get(chooser: (item: XrmBase.PageSection, index: number) => boolean): XrmBase.PageSection[];
            }
            interface Linkedinv2tab extends XrmBase.SectionCollectionBase {
                get(name: "linkedin_v2_tab_section_1"): XrmBase.PageSection;
                get(name: "linkedin_v2_tab_section_2"): XrmBase.PageSection;
                get(name: string): undefined;
                get(): XrmBase.PageSection[];
                get(index: number): XrmBase.PageSection;
                get(chooser: (item: XrmBase.PageSection, index: number) => boolean): XrmBase.PageSection[];
            }
            interface Conflictstab extends XrmBase.SectionCollectionBase {
                get(name: "conflictssection"): XrmBase.PageSection;
                get(name: string): undefined;
                get(): XrmBase.PageSection[];
                get(index: number): XrmBase.PageSection;
                get(chooser: (item: XrmBase.PageSection, index: number) => boolean): XrmBase.PageSection[];
            }
        }
        interface Attributes extends XrmBase.AttributeCollectionBase {
            get(name: "address1_city"): XrmBase.StringAttribute;
            get(name: "address1_county"): XrmBase.StringAttribute;
            get(name: "address1_line1"): XrmBase.StringAttribute;
            get(name: "address1_line2"): XrmBase.StringAttribute;
            get(name: "address1_postalcode"): XrmBase.StringAttribute;
            get(name: "address1_stateorprovince"): XrmBase.StringAttribute;
            get(name: "createdby"): XrmBase.LookupAttribute<"systemuser">;
            get(name: "createdon"): XrmBase.DateAttribute;
            get(name: "description"): XrmBase.StringAttribute;
            get(name: "donotbulkemail"): XrmBase.OptionSetAttribute<boolean>;
            get(name: "donotbulkpostalmail"): XrmBase.OptionSetAttribute<boolean>;
            get(name: "donotemail"): XrmBase.OptionSetAttribute<boolean>;
            get(name: "donotfax"): XrmBase.OptionSetAttribute<boolean>;
            get(name: "donotphone"): XrmBase.OptionSetAttribute<boolean>;
            get(name: "donotpostalmail"): XrmBase.OptionSetAttribute<boolean>;
            get(name: "door_buyeropportunities"): XrmBase.NumberAttribute;
            get(name: "door_lob_deliverability"): XrmBase.OptionSetAttribute<door_contact_door_lob_deliverability>;
            get(name: "door_market"): XrmBase.LookupAttribute<"territory">;
            get(name: "door_numberofclosedactivities"): XrmBase.NumberAttribute;
            get(name: "door_numberofopenactivities"): XrmBase.NumberAttribute;
            get(name: "door_selleropportunities"): XrmBase.NumberAttribute;
            get(name: "emailaddress1"): XrmBase.StringAttribute;
            get(name: "firstname"): XrmBase.StringAttribute;
            get(name: "jobtitle"): XrmBase.StringAttribute;
            get(name: "lastname"): XrmBase.StringAttribute;
            get(name: "mobilephone"): XrmBase.StringAttribute;
            get(name: "modifiedby"): XrmBase.LookupAttribute<"systemuser">;
            get(name: "modifiedon"): XrmBase.DateAttribute;
            get(name: "originatingleadid"): XrmBase.LookupAttribute<"lead">;
            get(name: "parentcustomerid"): XrmBase.LookupAttribute<"account" | "contact">;
            get(name: "statecode"): XrmBase.OptionSetAttribute<contact_statecode>;
            get(name: "statuscode"): XrmBase.OptionSetAttribute<contact_statuscode>;
            get(name: "telephone1"): XrmBase.StringAttribute;
            get(name: string): undefined;
            get(): XrmBase.Attribute<any>[];
            get(index: number): XrmBase.Attribute<any>;
            get(chooser: (item: XrmBase.Attribute<any>, index: number) => boolean): XrmBase.Attribute<any>[];
        }
        interface Controls extends XrmBase.ControlCollectionBase {
            get(name: "ActionCards"): XrmBase.BaseControl;
            get(name: "address1_city"): XrmBase.StringControl;
            get(name: "address1_county"): XrmBase.StringControl;
            get(name: "address1_line1"): XrmBase.StringControl;
            get(name: "address1_line2"): XrmBase.StringControl;
            get(name: "address1_postalcode"): XrmBase.StringControl;
            get(name: "address1_stateorprovince"): XrmBase.StringControl;
            get(name: "connections"): XrmBase.SubGridControl<"connection">;
            get(name: "createdby"): XrmBase.LookupControl<"systemuser">;
            get(name: "createdon"): XrmBase.DateControl;
            get(name: "description"): XrmBase.StringControl;
            get(name: "documentlocations"): XrmBase.SubGridControl<"sharepointdocumentlocation">;
            get(name: "donotbulkemail"): XrmBase.OptionSetControl<any>;
            get(name: "donotbulkpostalmail"): XrmBase.OptionSetControl<any>;
            get(name: "donotemail"): XrmBase.OptionSetControl<any>;
            get(name: "donotfax"): XrmBase.OptionSetControl<any>;
            get(name: "donotphone"): XrmBase.OptionSetControl<any>;
            get(name: "donotpostalmail"): XrmBase.OptionSetControl<any>;
            get(name: "door_buyeropportunities"): XrmBase.NumberControl;
            get(name: "door_lob_deliverability"): XrmBase.BaseControl;
            get(name: "door_market"): XrmBase.LookupControl<"territory">;
            get(name: "door_numberofclosedactivities"): XrmBase.NumberControl;
            get(name: "door_numberofopenactivities"): XrmBase.NumberControl;
            get(name: "door_selleropportunities"): XrmBase.NumberControl;
            get(name: "emailaddress1"): XrmBase.StringControl;
            get(name: "firstname"): XrmBase.StringControl;
            get(name: "jobtitle"): XrmBase.StringControl;
            get(name: "lastname"): XrmBase.StringControl;
            get(name: "leads"): XrmBase.SubGridControl<"lead">;
            get(name: "mapcontrol"): XrmBase.BaseControl;
            get(name: "mobilephone"): XrmBase.StringControl;
            get(name: "modifiedby"): XrmBase.LookupControl<"systemuser">;
            get(name: "modifiedon"): XrmBase.DateControl;
            get(name: "News"): XrmBase.BaseControl;
            get(name: "notescontrol"): XrmBase.StringControl;
            get(name: "offers"): XrmBase.SubGridControl<"door_offer">;
            get(name: "opportunities"): XrmBase.SubGridControl<"opportunity">;
            get(name: "originatingleadid"): XrmBase.LookupControl<"lead">;
            get(name: "parentcustomerid"): XrmBase.BaseControl;
            get(name: "propoertyshowings"): XrmBase.SubGridControl<"door_propertyshowings">;
            get(name: "showings"): XrmBase.SubGridControl<"door_showing">;
            get(name: "statecode"): XrmBase.OptionSetControl<contact_statecode>;
            get(name: "statuscode"): XrmBase.OptionSetControl<contact_statuscode>;
            get(name: "TalkingPoints"): XrmBase.BaseControl;
            get(name: "telephone1"): XrmBase.StringControl;
            get(name: string): undefined;
            get(): XrmBase.BaseControl[];
            get(index: number): XrmBase.BaseControl;
            get(chooser: (item: XrmBase.BaseControl, index: number) => boolean): XrmBase.BaseControl[];
        }
        interface Tabs extends XrmBase.TabCollectionBase {
            get(name: "SUMMARY_TAB"): XrmBase.PageTab<Tabs.SUMMARYTAB>;
            get(name: "tab_4"): XrmBase.PageTab<Tabs.Tab4>;
            get(name: "tab_5"): XrmBase.PageTab<Tabs.Tab5>;
            get(name: "tab_9"): XrmBase.PageTab<Tabs.Tab9>;
            get(name: "tab_10"): XrmBase.PageTab<Tabs.Tab10>;
            get(name: "tab_7"): XrmBase.PageTab<Tabs.Tab7>;
            get(name: "tab_8"): XrmBase.PageTab<Tabs.Tab8>;
            get(name: "tab_6"): XrmBase.PageTab<Tabs.Tab6>;
            get(name: "linkedin_v2_tab"): XrmBase.PageTab<Tabs.Linkedinv2tab>;
            get(name: "conflictstab"): XrmBase.PageTab<Tabs.Conflictstab>;
            get(name: string): undefined;
            get(): XrmBase.PageTab<XrmBase.Collection<XrmBase.PageSection>>[];
            get(index: number): XrmBase.PageTab<XrmBase.Collection<XrmBase.PageSection>>;
            get(chooser: (item: XrmBase.PageTab<XrmBase.Collection<XrmBase.PageSection>>, index: number) => boolean): XrmBase.PageTab<XrmBase.Collection<XrmBase.PageSection>>[];
        }
    }
    interface ContactExperimental extends XrmBase.PageBase<ContactExperimental.Attributes, ContactExperimental.Tabs, ContactExperimental.Controls> {
        getAttribute(attributeName: "firstname"): XrmBase.StringAttribute;
        getAttribute(attributeName: "lastname"): XrmBase.StringAttribute;
        getAttribute(attributeName: "emailaddress1"): XrmBase.StringAttribute;
        getAttribute(attributeName: "telephone1"): XrmBase.StringAttribute;
        getAttribute(attributeName: "mobilephone"): XrmBase.StringAttribute;
        getAttribute(attributeName: "jobtitle"): XrmBase.StringAttribute;
        getAttribute(attributeName: "door_market"): XrmBase.LookupAttribute<"territory">;
        getAttribute(attributeName: "door_buyeropportunities"): XrmBase.NumberAttribute;
        getAttribute(attributeName: "door_selleropportunities"): XrmBase.NumberAttribute;
        getAttribute(attributeName: "description"): XrmBase.StringAttribute;
        getAttribute(attributeName: "address1_line1"): XrmBase.StringAttribute;
        getAttribute(attributeName: "address1_line2"): XrmBase.StringAttribute;
        getAttribute(attributeName: "address1_city"): XrmBase.StringAttribute;
        getAttribute(attributeName: "address1_stateorprovince"): XrmBase.StringAttribute;
        getAttribute(attributeName: "address1_postalcode"): XrmBase.StringAttribute;
        getAttribute(attributeName: "address1_county"): XrmBase.StringAttribute;
        getAttribute(attributeName: "door_lob_deliverability"): XrmBase.OptionSetAttribute<door_contact_door_lob_deliverability>;
        getAttribute(attributeName: "createdon"): XrmBase.DateAttribute;
        getAttribute(attributeName: "createdby"): XrmBase.LookupAttribute<"systemuser">;
        getAttribute(attributeName: "modifiedon"): XrmBase.DateAttribute;
        getAttribute(attributeName: "modifiedby"): XrmBase.LookupAttribute<"systemuser">;
        getAttribute(attributeName: "door_numberofopenactivities"): XrmBase.NumberAttribute;
        getAttribute(attributeName: "door_numberofclosedactivities"): XrmBase.NumberAttribute;
        getAttribute(attributeName: "statecode"): XrmBase.OptionSetAttribute<contact_statecode>;
        getAttribute(attributeName: "statuscode"): XrmBase.OptionSetAttribute<contact_statuscode>;
        getAttribute(attributeName: "originatingleadid"): XrmBase.LookupAttribute<"lead">;
        getAttribute(attributeName: "donotphone"): XrmBase.OptionSetAttribute<boolean>;
        getAttribute(attributeName: "donotemail"): XrmBase.OptionSetAttribute<boolean>;
        getAttribute(attributeName: "donotpostalmail"): XrmBase.OptionSetAttribute<boolean>;
        getAttribute(attributeName: "donotbulkemail"): XrmBase.OptionSetAttribute<boolean>;
        getAttribute(attributeName: "donotbulkpostalmail"): XrmBase.OptionSetAttribute<boolean>;
        getAttribute(attributeName: "donotfax"): XrmBase.OptionSetAttribute<boolean>;
        getAttribute(attributeName: "parentcustomerid"): XrmBase.LookupAttribute<"account" | "contact">;
        getAttribute(attributeName: string): undefined;
        getControl(controlName: "firstname"): XrmBase.StringControl;
        getControl(controlName: "lastname"): XrmBase.StringControl;
        getControl(controlName: "emailaddress1"): XrmBase.StringControl;
        getControl(controlName: "telephone1"): XrmBase.StringControl;
        getControl(controlName: "mobilephone"): XrmBase.StringControl;
        getControl(controlName: "jobtitle"): XrmBase.StringControl;
        getControl(controlName: "door_market"): XrmBase.LookupControl<"territory">;
        getControl(controlName: "door_buyeropportunities"): XrmBase.NumberControl;
        getControl(controlName: "door_selleropportunities"): XrmBase.NumberControl;
        getControl(controlName: "description"): XrmBase.StringControl;
        getControl(controlName: "address1_line1"): XrmBase.StringControl;
        getControl(controlName: "address1_line2"): XrmBase.StringControl;
        getControl(controlName: "address1_city"): XrmBase.StringControl;
        getControl(controlName: "address1_stateorprovince"): XrmBase.StringControl;
        getControl(controlName: "address1_postalcode"): XrmBase.StringControl;
        getControl(controlName: "address1_county"): XrmBase.StringControl;
        getControl(controlName: "door_lob_deliverability"): XrmBase.BaseControl;
        getControl(controlName: "mapcontrol"): XrmBase.BaseControl;
        getControl(controlName: "News"): XrmBase.BaseControl;
        getControl(controlName: "TalkingPoints"): XrmBase.BaseControl;
        getControl(controlName: "notescontrol"): XrmBase.StringControl;
        getControl(controlName: "ActionCards"): XrmBase.BaseControl;
        getControl(controlName: "opportunities"): XrmBase.SubGridControl<"opportunity">;
        getControl(controlName: "leads"): XrmBase.SubGridControl<"lead">;
        getControl(controlName: "showings"): XrmBase.SubGridControl<"door_showing">;
        getControl(controlName: "offers"): XrmBase.SubGridControl<"door_offer">;
        getControl(controlName: "propoertyshowings"): XrmBase.SubGridControl<"door_propertyshowings">;
        getControl(controlName: "documentlocations"): XrmBase.SubGridControl<"sharepointdocumentlocation">;
        getControl(controlName: "createdon"): XrmBase.DateControl;
        getControl(controlName: "createdby"): XrmBase.LookupControl<"systemuser">;
        getControl(controlName: "modifiedon"): XrmBase.DateControl;
        getControl(controlName: "modifiedby"): XrmBase.LookupControl<"systemuser">;
        getControl(controlName: "door_numberofopenactivities"): XrmBase.NumberControl;
        getControl(controlName: "door_numberofclosedactivities"): XrmBase.NumberControl;
        getControl(controlName: "statecode"): XrmBase.OptionSetControl<contact_statecode>;
        getControl(controlName: "statuscode"): XrmBase.OptionSetControl<contact_statuscode>;
        getControl(controlName: "originatingleadid"): XrmBase.LookupControl<"lead">;
        getControl(controlName: "donotphone"): XrmBase.OptionSetControl<any>;
        getControl(controlName: "donotemail"): XrmBase.OptionSetControl<any>;
        getControl(controlName: "donotpostalmail"): XrmBase.OptionSetControl<any>;
        getControl(controlName: "donotbulkemail"): XrmBase.OptionSetControl<any>;
        getControl(controlName: "donotbulkpostalmail"): XrmBase.OptionSetControl<any>;
        getControl(controlName: "donotfax"): XrmBase.OptionSetControl<any>;
        getControl(controlName: "parentcustomerid"): XrmBase.BaseControl;
        getControl(controlName: "connections"): XrmBase.SubGridControl<"connection">;
        getControl(controlName: string): undefined;
    }
}
